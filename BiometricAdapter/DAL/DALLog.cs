﻿using BiometricAdapter.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Oracle.DataAccess.Client;
using System.Linq;
using System.Web;

namespace BiometricAdapter.DAL
{
    public class DALLog
    {
        OracleDataManager manager = new OracleDataManager();
        
        
        public int ApiRequestLogInsert(ApiRequestLog requestLog)
        {
            try
            {
                manager.AddParameter(new OracleParameter("p_loginprovider", requestLog.loginprovider));
                manager.AddParameter(new OracleParameter("p_methode_name", requestLog.methode_name));
                manager.AddParameter(new OracleParameter("p_request_json", requestLog.request_json));
                manager.AddParameter(new OracleParameter("p_response_json", requestLog.response_json));
                long? log_id = manager.CallStoredProcedure_Insert("USP_APIRequestLog_Insert");
                if (log_id.HasValue) return (int)log_id.Value;
                else return 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}