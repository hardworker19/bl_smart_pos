﻿using BiometricAdapter.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BiometricAdapter
{
    /// <summary>
    /// The Application startup class 
    /// </summary>
    public class WebApiApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// The method which call when application start
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GetAllUser1();
        }

        private void GetAllUser1()
        {
            BLLAspNetUser user = new BLLAspNetUser();
            user.GetAllUser();
        }

        /// <summary>
        /// An global method for custom authorization
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static bool IsAuthorized(HttpRequestMessage request)
        {
            var headers = request.Headers;

            if (headers.Contains("security_token"))
            {
                //TODO: Retrive user information and checking for provided security token
                return true;
            }
            else return false;
        }
    }
}
