// Uncomment the following to provide samples for PageResult<T>. Must also add the Microsoft.AspNet.WebApi.OData
// package to your project.
////#define Handle_PageResultOfT

using BiometricAdapter.Models;
using BiometricAdapter.RequestModels;
using BiometricAdapter.ResponseModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.Http;
#if Handle_PageResultOfT
using System.Web.Http.OData;
#endif

namespace BiometricAdapter.Areas.HelpPage
{
    /// <summary>
    /// Use this class to customize the Help Page.
    /// For example you can set a custom <see cref="System.Web.Http.Description.IDocumentationProvider"/> to supply the documentation
    /// or you can provide the samples for the requests/responses.
    /// </summary>
    public static class HelpPageConfig
    {
        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters",
            MessageId = "BiometricAdapter.Areas.HelpPage.TextSample.#ctor(System.String)",
            Justification = "End users may choose to merge this string with existing localized resources.")]
        [SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly",
            MessageId = "bsonspec",
            Justification = "Part of a URI.")]
        public static void Register(HttpConfiguration config)
        {
            //// Uncomment the following to use the documentation from XML documentation file.
            config.SetDocumentationProvider(new XmlDocumentationProvider(HttpContext.Current.Server.MapPath("~/App_Data/BiometricAdapter.xml")));

            // Uncomment the following to use "sample string" as the sample for all actions that have string as the body parameter or return type.
            // Also, the string arrays will be used for IEnumerable<string>. The sample objects will be serialized into different media type 
            // formats by the available formatters.
            GenerateSample(config);

            // Extend the following to provide factories for types not handled automatically (those lacking parameterless
            // constructors) or for which you prefer to use non-default property values. Line below provides a fallback
            // since automatic handling will fail and GeneratePageResult handles only a single type.
#if Handle_PageResultOfT
            config.GetHelpPageSampleGenerator().SampleObjectFactories.Add(GeneratePageResult);
#endif

            // Extend the following to use a preset object directly as the sample for all actions that support a media
            // type, regardless of the body parameter or return type. The lines below avoid display of binary content.
            // The BsonMediaTypeFormatter (if available) is not used to serialize the TextSample object.
            config.SetSampleForMediaType(
                new TextSample("Binary JSON content. See http://bsonspec.org for details."),
                new MediaTypeHeaderValue("application/bson"));

            //// Uncomment the following to use "[0]=foo&[1]=bar" directly as the sample for all actions that support form URL encoded format
            //// and have IEnumerable<string> as the body parameter or return type.
            //config.SetSampleForType("[0]=foo&[1]=bar", new MediaTypeHeaderValue("application/x-www-form-urlencoded"), typeof(IEnumerable<string>));

            //// Uncomment the following to use "1234" directly as the request sample for media type "text/plain" on the controller named "Values"
            //// and action named "Put".
            //config.SetSampleRequest("1234", new MediaTypeHeaderValue("text/plain"), "Values", "Put");

            //// Uncomment the following to use the image on "../images/aspNetHome.png" directly as the response sample for media type "image/png"
            //// on the controller named "Values" and action named "Get" with parameter "id".
            //config.SetSampleResponse(new ImageSample("../images/aspNetHome.png"), new MediaTypeHeaderValue("image/png"), "Values", "Get", "id");

            //// Uncomment the following to correct the sample request when the action expects an HttpRequestMessage with ObjectContent<string>.
            //// The sample will be generated as if the controller named "Values" and action named "Get" were having string as the body parameter.
            //config.SetActualRequestType(typeof(string), "Values", "Get");

            //// Uncomment the following to correct the sample response when the action returns an HttpResponseMessage with ObjectContent<string>.
            //// The sample will be generated as if the controller named "Values" and action named "Post" were returning a string.
            //config.SetActualResponseType(typeof(string), "Values", "Post");
        }

        /// <summary>
        /// Generate all samples
        /// </summary>
        /// <param name="config"></param>
        public static void GenerateSample(HttpConfiguration config)
        {
            config.SetSampleObjects(new Dictionary<Type, object>
            {
                {typeof(string), "sample string"},
                {typeof(IEnumerable<string>), new string[]{"sample 1", "sample 2"}},
                {typeof(LoginRequest), new LoginRequest(){ user_name="apiuser", password="ApiUser@123456"}},
                {typeof(ChangePasswordRequest), new ChangePasswordRequest(){ old_password="OldApiUser@123456", new_password="NewApiUser@123456"}},
                {typeof(COMMSISDNRequest), new COMMSISDNRequest(){  msisdn = "88019XXXXXXXX"}},
                {typeof(MSISDNFile), new MSISDNFile(){  msisdn = "88019XXXXXXXX",file_name="Fingerprint.png"}},
                {typeof(IndividualRequest), new IndividualRequest()
                    {
                         user_name = "apiuser",
                         user_type = "RETAILER",
                         source = "BSS API",
                         bss_order_id = "BSS0000000001",
                         msisdn = "88019XXXXXXXX",
                         dest_imsi = "12345678901234",
                         purpose_no = 2,
                         dest_doc_type_no = 1,
                         dest_doc_id = "12345678901234567",
                         dest_dob = "31/12/1980",
                         is_verified = 1,
                         is_sim_category_changed = 0,
                         src_sim_category = 0,
                         dest_sim_category = 1,
                         reg_date=DateTime.Now.ToString("dd/MM/YYYY"),
                         dest_foreign_flag = 0,
                         dest_exp_time = "03.00",
                         src_doc_type_no = "NID",
                         src_doc_id = "25698745125",
                         src_dob = "31/12/1980",
                         //src_left_thumb =null,
                         //src_left_index = null,
                         //src_right_thumb = null,
                         //src_right_index = null,
                         src_left_thumb_score = 97,
                         src_left_index_score = 80,
                         src_right_thumb_score = 90,
                         src_right_index_score = 85,
                         //dest_left_thumb = null,
                         //dest_left_index = null,
                         //dest_right_thumb = null,
                         //dest_right_index = null,
                         dest_left_thumb_score = 98,
                         dest_left_index_score = 96,
                         dest_right_thumb_score = 0,
                         dest_right_index_score = 95
                    }},
                {typeof(CorporateRequest), new CorporateRequest()
                    {
                        user_name="apiuser",
                        user_type="RETAILER",
                        source="BSS API",
                        bss_order_id=0000000001,//in individul this feld is string but here this feld is int
                        msisdn="88019XXXXXXXX",
                        poc_msisdn = "88019XXXXXXXX",
                        dest_imsi="12345678901234",
                        purpose_no=2,
                        dest_poc_doc_type_no=1,
                        dest_poc_doc_id="12345678901234567",
                        dest_poc_dob="31/12/1980",
                        is_verified=1,
                        is_sim_category_changed=0,
                        dest_sim_category = 15,
                        src_sim_category=1,
                        reg_date=DateTime.Now.ToString("dd/MM/YYYY"),
                        dest_foreign_flag=0,
                        dest_exp_time="03.00",
                        src_poc_doc_type_no = "NID",
                        src_poc_doc_id = "1236598745896",
                        src_poc_dob = "31/12/1980",
                        //src_poc_left_thumb = "QEA=",
                        //src_poc_left_index = "QEA=",
                        //src_poc_right_thumb = "QEA=",
                        //src_poc_right_index = "QEA=",
                        src_poc_left_thumb_score = 22,
                        src_poc_left_index_score = 23,
                        src_poc_right_thumb_score = 24,
                        src_poc_right_index_score = 25,
                        //dest_poc_left_thumb = "QEA=",
                        //dest_poc_left_index = "QEA=",
                        //dest_poc_right_thumb = "QEA=",
                        //dest_poc_right_index = "QEA=",
                        dest_poc_left_thumb_score = 26,
                        dest_poc_left_index_score = 27,
                        dest_poc_right_thumb_score= 28,
                        dest_poc_right_index_score= 29
                    }},
                #region Response Area
                {typeof(IndividualResponse), new IndividualResponse(){  is_api_success = true,message="OK",data=new IndividualData(){  is_success = true,message="Operation Successful",error_code=null,description=null,token_no=1236540}}},
                {typeof(CorporateResponse), new CorporateResponse(){  is_api_success = true,message="OK",data=new CorporateData(){  is_success = true,message="Operation Successful",error_code=null,description=null,token_no=1236540}}},
                {typeof(GetStatusResponse), new GetStatusResponse(){  is_api_success = true,message="OK",data=new GetStatusData(){  is_success = true,message="Success",error_code=null,description=null,msisdn="88019XXXXXXXX",token_no=1236540,status="40"}}},
                {typeof(SAFSaveResponse), new SAFSaveResponse(){  is_api_success = true,message="OK",data=new SAFSaveData(){  is_success = true,message="File save successfully",error_code=null,description=null}}},
                {typeof(SAFGetFilesResponse), new SAFGetFilesResponse(){  is_api_success = true,message="OK",data=new SAFGetFilesData(){  is_success = true,message="Success",error_code=null,description=null,files=null}}},
                {typeof(LoginResponse), new LoginResponse(){  is_api_success = true,message="OK",data=new LoginData(){  is_success = true,message="Login Successful",error_code=null,description=null,session_token="0125da81-dcb7-4186-a9a9-e55be730b585"}}},
                {typeof(ChangePasswordResponse), new ChangePasswordResponse(){  is_api_success = true,message="OK",data=new ChangePasswordData(){  is_success = true,message="Password Changed",error_code=null,description=null}}},
	            #endregion
            });
        }

#if Handle_PageResultOfT
        private static object GeneratePageResult(HelpPageSampleGenerator sampleGenerator, Type type)
        {
            if (type.IsGenericType)
            {
                Type openGenericType = type.GetGenericTypeDefinition();
                if (openGenericType == typeof(PageResult<>))
                {
                    // Get the T in PageResult<T>
                    Type[] typeParameters = type.GetGenericArguments();
                    Debug.Assert(typeParameters.Length == 1);

                    // Create an enumeration to pass as the first parameter to the PageResult<T> constuctor
                    Type itemsType = typeof(List<>).MakeGenericType(typeParameters);
                    object items = sampleGenerator.GetSampleObject(itemsType);

                    // Fill in the other information needed to invoke the PageResult<T> constuctor
                    Type[] parameterTypes = new Type[] { itemsType, typeof(Uri), typeof(long?), };
                    object[] parameters = new object[] { items, null, (long)ObjectGenerator.DefaultCollectionSize, };

                    // Call PageResult(IEnumerable<T> items, Uri nextPageLink, long? count) constructor
                    ConstructorInfo constructor = type.GetConstructor(parameterTypes);
                    return constructor.Invoke(parameters);
                }
            }

            return null;
        }
#endif
    }
}