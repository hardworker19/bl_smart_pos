﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.RequestModels
{
    /// <summary>
    /// The model for managing authentication from external user for requesting all APIs
    /// </summary>
    public class LoginRequest
    {
        /// <summary>
        /// User name for request
        /// </summary>
        public string user_name { get; set; }

        /// <summary>
        /// The authentication password
        /// </summary>
        public string password { get; set; }
        
    }
    
}