﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.RequestModels
{
    /// <summary>
    /// The API Model for passing token number when requesting for transaction
    /// </summary>
    public class COMTokenNoRequest
    {
        /// <summary>
        /// The token number provided when requesting for transaction
        /// </summary>
        public long token_no { get; set; }
    }
}