﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.RequestModels
{
    /// <summary>
    /// The model for changing authentication password from external user
    /// </summary>
    public class ChangePasswordRequest
    {
        /// <summary>
        /// The password which currently used by user
        /// </summary>
        public string old_password { get; set; }
        /// <summary>
        /// The new password which will be applied for next login
        /// </summary>
        public string new_password { get; set; }
    }
}