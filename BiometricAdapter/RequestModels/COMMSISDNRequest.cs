﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.RequestModels
{
    /// <summary>
    /// The API Model for passing mobile number (MSISDN) when requesting for transaction
    /// </summary>
    public class COMMSISDNRequest
    {
        /// <summary>
        /// The mobile number with 880
        /// </summary>
        public string msisdn { get; set; }
    }
}