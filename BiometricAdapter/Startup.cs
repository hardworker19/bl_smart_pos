﻿using System;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BiometricAdapter.App_Start.Startup))]
namespace BiometricAdapter.App_Start
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
