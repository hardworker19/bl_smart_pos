﻿using BiometricAdapter.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BiometricAdapter.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        /// <summary>
        /// test Action 
        /// </summary>
        /// <returns></returns>
        //[AuthorizationFilter]
        public JsonResult MyAction()
        {
            return Json("success", JsonRequestBehavior.AllowGet);
        }
    }
}
