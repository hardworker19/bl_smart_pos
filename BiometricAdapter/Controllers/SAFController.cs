﻿using BiometricAdapter.Models;
using BiometricAdapter.RequestModels;
using BiometricAdapter.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BiometricAdapter.Controllers
{
    /// <summary>
    /// MSISDN wise Documents upload and view APIs
    /// </summary>
    public class SAFController : ApiController
    {
        // GET: api/SAF
        /// <summary>
        /// Get a list of files which are available for requested MSISDN
        /// </summary>
        /// <param name="mssidnRequest">The mobile number with 880</param>
        /// <returns>A list of files</returns>
        [HttpPost]
        [Route("api/SAF/GetFiles")]
        public SAFGetFilesResponse GetFiles([FromBody]COMMSISDNRequest mssidnRequest)
        {
            if (!BiometricAdapter.WebApiApplication.IsAuthorized(Request))
            {
                return new SAFGetFilesResponse()
                {
                    is_api_success = true,
                    message = "success",
                    data = new SAFGetFilesData()
                    {
                        is_success = false,
                        message = "Invalid authentication, please contact with administrator",
                        error_code = "SEC0001",
                        description = "Session time out"
                    }
                };
            }

            //TODO: Implement functionality
            return new SAFGetFilesResponse()
            {
                is_api_success = true,
                message = "success",
                data = new SAFGetFilesData()
                {
                    is_success = true,
                    message = "File save successfully",
                    files = new List<MSISDNFile>()
                }
            };
        }



        // POST: api/SAF
        /// <summary>
        /// Request for upload and save anykinds of file for MSISDN
        /// </summary>
        /// <param name="msisdnFile">The object of MSISDN with a file for save </param>
        /// <returns>Success or Failed result</returns>
        [Route("api/SAF/Save")]
        public SAFSaveResponse Save([FromBody]MSISDNFile msisdnFile)
        {
            if (!BiometricAdapter.WebApiApplication.IsAuthorized(Request))
            {
                return new SAFSaveResponse()
                {
                    is_api_success = true,
                    message = "success",
                    data = new SAFSaveData()
                    {
                        is_success = false,
                        message = "Invalid authentication, please contact with administrator",
                        error_code = "SEC0001",
                        description = "Session time out"
                    }
                };
            }

            //TODO: Implement functionality
            return new SAFSaveResponse()
            {
                is_api_success = true,
                message = "success",
                data = new SAFSaveData()
                {
                    is_success = true,
                    message = "File save successfully"
                }
            };
        }
        
    }
}
