﻿using BiometricAdapter.RequestModels;
using BiometricAdapter.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace BiometricAdapter.Controllers
{
    /// <summary>
    /// Managing security for all APIs in Biometric Smart POS Solution
    /// </summary>
    public class SecurityController : ApiController
    {


        // POST: api/Security/Login
        /// <summary>
        /// Authentication API for external user
        /// </summary>
        /// <param name="loginInfo">Requesting parameter with username and password</param>
        /// <returns>Return the authentication information of requesting user</returns>
        [Route("api/Security/Login")]
        public LoginResponse Login([FromBody]LoginRequest loginInfo)
        {
            return new LoginResponse()
            {
                is_api_success = true,
                message = "success",
                data = new LoginData() {
                    is_success = true,
                    message = "success",
                    session_token = Guid.NewGuid().ToString()
                }
            };
        }

        /// <summary>
        /// API for change password
        /// </summary>
        /// <param name="changePassword">Requesting parameter with old password and new password</param>
        /// <returns>Return reuslt of logout request</returns>
        [Route("api/Security/ChangePassword")]
        public ChangePasswordResponse ChangePassword([FromBody]ChangePasswordRequest changePassword)
        {
            if (!BiometricAdapter.WebApiApplication.IsAuthorized(Request))
            {
                return new ChangePasswordResponse()
                {
                    is_api_success = true,
                    message = "success",
                    data=new ChangePasswordData()
                    {
                        is_success=false,
                        message="Invalid authentication, please contact with administrator",
                        error_code="SEC0001",
                        description = "Session time out"
                    }
                };
            }

            //TODO: Implement functionality
            return new ChangePasswordResponse()
            {
                is_api_success = true,
                message = "success",
                data = new ChangePasswordData()
                {
                    is_success = true,
                    message = "Password changed successfully"
                }
            };
        }


        

    }
}
