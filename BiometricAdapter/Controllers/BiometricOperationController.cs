﻿using BiometricAdapter.RequestModels;
using BiometricAdapter.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BiometricAdapter.Controllers
{
    /// <summary>
    /// All biometric (CBVMP and EC) verification will process by these API area for all kinds of operation (Individual and corporate)
    /// </summary>
    //[Route("BiometricOperation")]
    public class BiometricOperationController : ApiController
    {


        // POST: api/BiometricOperationIndividual
        /// <summary>
        /// This API is called from BSS, TABS, DPOS to All biometric (CBVMP and EC) verification will process by these API area for all kinds of individual operation
        /// </summary>
        /// <param name="individualRequest">The object with all parameters which are passing by API request required for verification</param>
        /// <returns>The object with all parameters which are response after processing the request</returns>
        [HttpPost]
        [Route("api/BiometricOperation/Individual")]
        public IndividualResponse Individual([FromBody]IndividualRequest individualRequest)
        {
            if (!BiometricAdapter.WebApiApplication.IsAuthorized(Request))
            {
                return new IndividualResponse()
                {
                    is_api_success = true,
                    message = "success",
                    data = new IndividualData()
                    {
                        is_success = false,
                        message = "Invalid authentication, please contact with administrator",
                        error_code = "SEC0001",
                        description = "Session time out"
                    }
                };
            }

            //TODO: Implement functionality
            return new IndividualResponse()
            {
                is_api_success = true,
                message = "success",
                data = new IndividualData()
                {
                    is_success = true,
                    message = "success",
                    token_no =Convert.ToInt64(String.Concat("1", DateTime.Now.ToString("YYYYMMDD"),"000001"))
                }
            };
        }

        // POST: api/BiometricOperationCorporate
        /// <summary>
        /// This API is called from BSS, TABS, DPOS to All biometric (CBVMP and EC) verification will process by these API area for all kinds of corporate operation
        /// </summary>
        /// <param name="corporateRequest">The object with all parameters which are passing by API request required for verification</param>
        /// <returns>The object with all parameters which are response after processing the request</returns>
        [HttpPost]
        [Route("api/BiometricOperation/Corporate")]
        public CorporateResponse Corporate([FromBody]CorporateRequest corporateRequest)
        {
            if (!BiometricAdapter.WebApiApplication.IsAuthorized(Request))
            {
                return new CorporateResponse()
                {
                    is_api_success = true,
                    message = "success",
                    data = new CorporateData()
                    {
                        is_success = false,
                        message = "Invalid authentication, please contact with administrator",
                        error_code = "SEC0001",
                        description = "Session time out"
                    }
                };
            }

            //TODO: Implement functionality
            return new CorporateResponse()
            {
                is_api_success = true,
                message = "success",
                data = new CorporateData()
                {
                    is_success = true,
                    message = "success",
                    token_no = Convert.ToInt64(String.Concat("2", DateTime.Now.ToString("YYYYMMDD"), "000001"))
                }
            };
        }

        /// <summary>
        /// This API is called for current status information based in BSS order id
        /// </summary>
        /// <param name="tokenNoRequest">The Object of token number provided when requesting for transaction</param>
        /// <returns>The object with status related information</returns>
        [HttpPost]
        [Route("api/BiometricOperation/GetStatus")]
        public GetStatusResponse GetStatus([FromBody]COMTokenNoRequest tokenNoRequest)
        {
            if (!BiometricAdapter.WebApiApplication.IsAuthorized(Request))
            {
                return new GetStatusResponse()
                {
                    is_api_success = true,
                    message = "success",
                    data = new GetStatusData()
                    {
                        is_success = false,
                        message = "Invalid authentication, please contact with administrator",
                        error_code = "SEC0001",
                        description = "Session time out"
                    }
                };
            }

            //TODO: Implement functionality
            return new GetStatusResponse()
            {
                is_api_success = true,
                message = "success",
                data = new GetStatusData()
                {
                    is_success = true,
                    message = "success",
                    msisdn="8801900000000",
                    token_no= tokenNoRequest.token_no,
                }
            };
        }
    }
}
