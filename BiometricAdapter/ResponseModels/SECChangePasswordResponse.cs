﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.ResponseModels
{
    /// <summary>
    /// The result of password changing request 
    /// </summary>
    public class ChangePasswordResponse : ResponseData
    {
        /// <summary>
        /// The response information of password changing request 
        /// </summary>
        public ChangePasswordData data { get; set; }
    }

    /// <summary>
    /// The implementation of absract class named Data
    /// </summary>
    public class ChangePasswordData : Data
    {
        
    }

}