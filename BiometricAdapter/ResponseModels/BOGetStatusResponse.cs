﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.ResponseModels
{
    /// <summary>
    /// Status Model for Biometric adapter operation 
    /// </summary>
    public class GetStatusResponse : ResponseData
    {
        /// <summary>
        /// The status information of request 
        /// </summary>
        public GetStatusData data { get; set; }
    }

    /// <summary>
    /// The implementation of absract class named Data with new column(s)
    /// </summary>
    public class GetStatusData : Data
    {
        /// <summary>
        /// The mobile number
        /// </summary>
        public string msisdn { get; set; }
        /// <summary>
        /// The token no which provided for farther status update
        /// </summary>
        public long token_no { get; set; }
        /// <summary>
        /// Current status of requested order id
        /// </summary>
        public string status { get; set; }
    }
}