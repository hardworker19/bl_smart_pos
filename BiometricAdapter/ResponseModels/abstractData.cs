﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.ResponseModels
{
    /// <summary>
    /// The Abstract response model after processing API request
    /// </summary>
    public abstract class Data
    {
        /// <summary>
        /// Define Operational level passed or failed result of the provided request. Always return true or false. Default value is false
        /// </summary>
        public bool is_success { get; set; }
        /// <summary>
        /// The message for operation success or failed.
        /// </summary>
        public string message { get; set; }
        /// <summary>
        /// The specified code for error identification
        /// </summary>
        public string error_code { get; set; }
        /// <summary>
        /// The specified error message which is pre-de
        /// </summary>
        public string description { get; set; }
    }
}