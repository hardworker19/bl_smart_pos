﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.ResponseModels
{
    /// <summary>
    /// The response of SAF file save based on MSISDN request 
    /// </summary>
    public class SAFSaveResponse : ResponseData
    {
        /// <summary>
        /// The result of SAF file save based on MSISDN request 
        /// </summary>
        public SAFSaveData data { get; set; }
    }

    /// <summary>
    /// The implementation of absract class named Data
    /// </summary>
    public class SAFSaveData : Data
    {
        
    }
}