﻿using BiometricAdapter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.ResponseModels
{
    /// <summary>
    /// The response of SAF file list based on MSISDN request 
    /// </summary>
    public class SAFGetFilesResponse : ResponseData
    {
        /// <summary>
        /// The result of SAF file list based on MSISDN request 
        /// </summary>
        public SAFGetFilesData data { get; set; }
    }

    /// <summary>
    /// The implementation of absract class named Data
    /// </summary>
    public class SAFGetFilesData : Data
    {
        /// <summary>
        /// A list of files
        /// </summary>
        public List<MSISDNFile> files { get; set; }
    }
}