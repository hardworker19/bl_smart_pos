﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.ResponseModels
{
    /// <summary>
    /// The authentication response of login request 
    /// </summary>
    public class LoginResponse : ResponseData
    {
        /// <summary>
        /// The authentication information of login request 
        /// </summary>
        public LoginData data { get; set; }
    }

    /// <summary>
    /// The implementation of absract class named Data with new column(s)
    /// </summary>
    public class LoginData : Data
    {
        /// <summary>
        /// The encrypted session token for farther API access
        /// </summary>
        public string session_token { get; set; }
    }
}