﻿
namespace BiometricAdapter.ResponseModels
{
    
    /// <summary>
    /// All properties for response after verification an corporate request process
    /// </summary>
    public class CorporateResponse : ResponseData
    {
        /// <summary>
        /// The response information of Biometric Operation - > Corporate request 
        /// </summary>
        public CorporateData data { get; set; }
    }

    /// <summary>
    /// The implementation of absract class named Data with new column(s)
    /// </summary>
    public class CorporateData : Data
    {
        /// <summary>
        /// The token no which provided for farther status update. 
        /// GENERATED FORMAT: [1 for INDIVIDUAL or 2 for CORPORATE][CURRENT DATE with format as YYYYMMDD][LAST SIX DIGIT with max TOKEN NO + 1]
        /// SAMPLE FOR CORPORATE: 220190627000004 where last one is 120190627000003
        /// </summary>
        public long token_no { get; set; }
    }
}