﻿
namespace BiometricAdapter.ResponseModels
{
    
    /// <summary>
    /// All properties for response after verification an individual request process
    /// </summary>
    public class IndividualResponse : ResponseData
    {
        /// <summary>
        /// The response information of Biometric Operation - > Individual request 
        /// </summary>
        public IndividualData data { get; set; }
    }

    /// <summary>
    /// The implementation of absract class named Data with new column(s)
    /// </summary>
    public class IndividualData : Data
    {
        /// <summary>
        /// The token no which provided for farther status update. 
        /// GENERATED FORMAT: [1 for INDIVIDUAL or 2 for CORPORATE][CURRENT DATE with format as YYYYMMDD][LAST SIX DIGIT with max TOKEN NO + 1]
        /// SAMPLE FOR INDIVIDUAL: 120190627000004 where last one is 120190627000003
        /// </summary>
        public long token_no { get; set; }
    }
}