﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.Models
{
    /// <summary>
    /// The model for managing authentication from external user for requesting all APIs
    /// </summary>
    public class Login
    {
        /// <summary>
        /// User name for request
        /// </summary>
        public string user_name { get; set; }

        /// <summary>
        /// The authentication password
        /// </summary>
        public string password { get; set; }
        
    }

    /// <summary>
    /// The model for changing authentication password from external user
    /// </summary>
    public class ChangePassword
    {
        /// <summary>
        /// The password which currently used by user
        /// </summary>
        public string old_password { get; set; }
        /// <summary>
        /// The new password which will be applied for next login
        /// </summary>
        public string new_password { get; set; }
    }
}