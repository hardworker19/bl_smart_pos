﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.Models
{
    /// <summary>
    /// All properties for manage MSISDN wise file save and view
    /// </summary>
    public class MSISDNFile
    {
        /// <summary>
        /// The mobile number with 880
        /// </summary>
        public string msisdn { get; set; }
        /// <summary>
        /// The name of file with extension (e.g. MyFile.pdf)
        /// </summary>
        public string file_name { get; set; }
        /// <summary>
        /// The binary (byte[]) data of any file
        /// </summary>
        public byte[] file { get; set; }
    }
}