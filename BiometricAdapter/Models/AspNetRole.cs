﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.Models
{
    /// <summary>
    /// Role Table
    /// </summary>
    public class AspNetRole
    {
        /// <summary>
        /// Role unique id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Role name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// User navigation property (UserList)
        /// </summary>
        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }

    }
}