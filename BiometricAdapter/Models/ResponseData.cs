﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.Models
{
    /// <summary>
    /// The model for common response for each request
    /// </summary>
    public class ResponseData
    {
        /// <summary>
        /// Define passed or failed result of the provided request. Always return true or false. Default value is false
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Define the success or error message of provided request processing result
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The various type of data as result of provided request
        /// </summary>
        public string Data { get; set; }
    }
}