﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiometricAdapter.Models
{
    /// <summary>
    /// Status Model for Biometric adapter operation 
    /// </summary>
    public class StatusResponse
    {
        /// <summary>
        /// Execution date time 
        /// </summary>
        public DateTime exe_date_time { get; set; }

        /// <summary>
        /// The mobile number
        /// </summary>
        public string msisdn { get; set; }

        /// <summary>
        /// Document Identification Number like NID number, PASSPORT number etc
        /// </summary>
        public string doc_id { get; set; }

        /// <summary>
        /// The token no which provided for farther status update
        /// </summary>
        public int token_no { get; set; }

        /// <summary>
        /// Current status of requested order id
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// The date of birth of customer as following format(dd/mm/yyyy)
        /// </summary>
        public string dob { get; set; }

    }
}