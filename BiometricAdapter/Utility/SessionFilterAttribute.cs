﻿
using BiometricAdapter.Models;
using System;
using System.Diagnostics.Contracts;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace BiometricAdapter.Utility
{
    /// <summary>
    /// Overwrite Authorize Attribute
    /// </summary>
    public class AuthorizationFilter : AuthorizeAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            Contract.Assert(filterContext != null);

            if(!(filterContext.ActionDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Length>0
               || filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Length>0))
            {
                HttpRequestBase request = filterContext.RequestContext.HttpContext.Request;
                string token = request.Headers["securityToken"];
                try
                {
                if (token==null)
                {
                        filterContext.Result = new HttpUnauthorizedResult();
                    }
                else
                {
                    AspNetUser user = ApiManager.ValidUserBySecurityToken(token);
                        if (user == null)
                        {
                            //filterContext.Result = new HttpUnauthorizedResult();
                        }
                        else
                        {
                            filterContext.Result = new HttpUnauthorizedResult();
                        }
                }
                }
                catch (Exception ex)
                {
                    //filterContext.Result = new HttpUnauthorizedResult();
                    filterContext.Result = new HttpUnauthorizedResult("Unauthorized user");
                    //throw new Exception("Un");
                }
            }
        }
    }
}