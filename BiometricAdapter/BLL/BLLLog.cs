﻿using BiometricAdapter.DAL;
using BiometricAdapter.Models;
using BiometricAdapter.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BiometricAdapter.BLL
{
    public class BLLLog
    {
        
        
       
        public void ApiRequestLogCreate(ApiRequestLog log)
        {
            DALLog dal = new DALLog();
            int request_log_id = dal.ApiRequestLogInsert(log);
            log.request_log_id = request_log_id;
        }

    }
}