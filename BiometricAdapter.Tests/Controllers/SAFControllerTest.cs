﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BiometricAdapter;
using BiometricAdapter.Controllers;
using BiometricAdapter.Models;
using BiometricAdapter.ResponseModels;
using BiometricAdapter.RequestModels;

namespace BiometricAdapter.Tests.Controllers
{
    [TestClass]
    public class SAFControllerTest
    {
        [TestMethod]
        public void Save()
        {
            // Arrange
            SAFController controller = new SAFController();

            // Act
            controller.Save(new MSISDNFile() { msisdn = "sample string 1", file = null });

            // Assert
        }

        [TestMethod]
        public void GetFiles()
        {
            // Arrange
            SAFController controller = new SAFController();

            // Act
            SAFGetFilesResponse result = controller.GetFiles(new COMMSISDNRequest() { msisdn="8801900000000"});

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.data.files.Count());
            //Assert.AreEqual("value1", result.data.files.ElementAt(0));
            //Assert.AreEqual("value2", result.data.files.ElementAt(1));
        }

        

        
        
    }
}
